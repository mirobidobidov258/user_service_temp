package service

import (
	"context"
	"fmt"

	pbp "template-service/genproto/product"
	pb "template-service/genproto/user"
	l "template-service/pkg/logger"
	grpcClient "template-service/service/grpc_client"
	"template-service/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UserService ...
type UserService struct {
	storage storage.IStorage
	logger  l.Logger
	client  grpcClient.GrpcClientI
}

// NewUserService ...
func NewUserService(db *sqlx.DB, log l.Logger, client grpcClient.GrpcClientI) *UserService {
	return &UserService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
	}
}

func (s *UserService) Create(ctx context.Context, req *pb.User) (*pb.User, error) {
	user, err := s.storage.User().Create(req)
	if err != nil {
		s.logger.Error("error INSERT", l.Any("error INSERT user", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}

	return user, nil
}

func (s *UserService) GetUserID(ctx context.Context, req *pb.UserID) (*pb.User, error) {
	users, err := s.storage.User().GetUserID(req.Id)
	if err != nil {
		s.logger.Error("error get users", l.Any("error while getting users user", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}
	products, err := s.client.Product().GetUserProducts(ctx, &pbp.GetUserProductsRequest{
		OwnerId: req.Id, 
	})
	if err != nil {
		s.logger.Error("error get products", l.Any("error while getting products info", err))
		return &pb.User{}, status.Error(codes.Internal, "something went wrong, please check product info")
	}
	for _, p := range products.Product {
		users.Products = append(users.Products, &pb.Prodct{
			Id:      p.Id,
			Name:    p.Name,
			Model:   p.Model,
			OwnerId: users.Id,
		})
	}
	users.ProductsCount = products.Count
	return users, nil
}   

func (s *UserService) GetTypeID(ctx context.Context, req *pb.TypeID) (*pb.Type, error) {
	types, err := s.storage.User().GetTypeID(req.Id)
	if err != nil{
		s.logger.Error("error get type", l.Any("error while getting type user", err))
		return &pb.Type{}, status.Error(codes.Internal, "something went wrong, please check user info")	
	}

	categorys, err := s.client.Product().GetTypeCategorys(ctx, &pbp.GetTypeCategoryRequest{
		TypeId: req.Id,
	})
	fmt.Println(categorys)
	if err != nil{
		s.logger.Error("error get category", l.Any("error while getting category user", err))
		return &pb.Type{}, status.Error(codes.Internal, "something went wrong, please check user info")	
	}
	for _, p := range categorys.Category{
		types.Category = append(types.Category, &pb.Category{
			Id: p.Id,
			Name: p.Name,
			TypeId: types.Id,
		})
	}
	return types, nil
}