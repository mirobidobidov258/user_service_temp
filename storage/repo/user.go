package repo

import (
    pb "template-service/genproto/user"
)

//UserStorageI ...
type UserStorageI interface {
    Create(*pb.User) (*pb.User, error)
    GetUserID(userID int64) (*pb.User, error)
    GetTypeID(typeID int64) (*pb.Type, error)
}