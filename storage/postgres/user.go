package postgres

import (
	"fmt"
	pb "template-service/genproto/user"

	"github.com/jmoiron/sqlx"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) *userRepo {
	return &userRepo{db: db}
}

func (r *userRepo) Create(user *pb.User) (*pb.User, error) {
	userResp := pb.User{}
	err := r.db.QueryRow(`insert into users (name, last_name) values($1, $2) 
	returning id, name, last_name`, user.Name, user.LastName).Scan(&userResp.Id, &userResp.Name, &userResp.LastName)
    if err != nil {
        return &pb.User{}, err
    }

	return &userResp, nil
}

func (r *userRepo) GetUserID(userID int64) (*pb.User, error) {
	fmt.Println(userID)
	userRepo := pb.User{}
	_, err := r.db.Exec(`select id, name, last_name from users where id=$1`,userID)
	if err != nil{
		return &pb.User{}, err
	}
	return &userRepo, nil
}

func (r *userRepo) GetTypeID(typeID int64) (*pb.Type, error) {
	fmt.Println(typeID)
	typeRepo := pb.Type{}
	_, err := r.db.Exec(`select id, name from type where id = $1`,typeID)
	if err != nil{
		return &pb.Type{}, err
	}
	return &typeRepo, nil
}

